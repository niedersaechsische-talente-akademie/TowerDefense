package de.unihannover.talenteakademie.towerdefense;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

import de.unihannover.talenteakademie.towerdefense.util.GameState;
import lombok.Getter;
import lombok.Setter;

/**
 * Entry point for the TowerDefenseGame libGDX application.
 */
public class TowerDefenseGame extends Game {

    @Getter
    private static TowerDefenseGame instance;

    @Getter
    private int timeMillis;

    @Getter
    @Setter
    private GameState gameState;

    @Getter
    private Music backgroundMusic;

    @Override
    public void create() {
        instance = this;

        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/kalimba.mp3"));
        backgroundMusic.setLooping(true);
        backgroundMusic.play();

        this.gameState = GameState.MENU;
        setScreen(gameState.getScreen());
    }

    @Override
    public void render() {
        this.timeMillis += Gdx.graphics.getDeltaTime() * 1000;

        if (!getScreen().equals(gameState.getScreen())) setScreen(gameState.getScreen());

        super.render();
    }
}
