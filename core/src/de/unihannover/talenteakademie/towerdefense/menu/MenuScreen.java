package de.unihannover.talenteakademie.towerdefense.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.util.Button;
import de.unihannover.talenteakademie.towerdefense.util.GameState;
import lombok.Getter;

/**
 * Created by Marco on 28.07.15.
 */
public class MenuScreen extends ScreenAdapter {

    @Getter
    private OrthographicCamera camera;
    private SpriteBatch batch;

    private Button startButton = new Button(266, 95, 269, 70);
    //private TextField textField;
    private Texture menuOverlay, background;

    @Override
    public void show() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        background = new Texture("grassbackground.png");
        menuOverlay = new Texture("menuoverlay.png");
        //textField = new TextField("Enter your name", new Skin(Gdx.files.internal()));
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (Gdx.input.justTouched()) checkInput();

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(background, 0, 0);
        batch.draw(menuOverlay, 0, 0);
        //textField.draw(batch, 1);

        batch.end();
    }

    private void checkInput() {
        Vector3 touchPosition = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));

        if (startButton.getArea().contains(touchPosition.x, touchPosition.y)) {
            TowerDefenseGame.getInstance().setGameState(GameState.RUNNING);
        }
    }
}
