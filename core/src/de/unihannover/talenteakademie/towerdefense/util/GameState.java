package de.unihannover.talenteakademie.towerdefense.util;

import com.badlogic.gdx.Screen;

import de.unihannover.talenteakademie.towerdefense.game.GameScreen;
import de.unihannover.talenteakademie.towerdefense.menu.MenuScreen;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Good old GameState enum.
 */
@Getter
@RequiredArgsConstructor
public enum GameState {

    MENU(new MenuScreen()), RUNNING(new GameScreen());

    private static final GameScreen GAME_SCREEN = new GameScreen();

    private final Screen screen;
}
