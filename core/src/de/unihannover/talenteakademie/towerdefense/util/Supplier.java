package de.unihannover.talenteakademie.towerdefense.util;

/**
 * Created by Marco on 27.07.15.
 */
public interface Supplier<T> {

    T get();
}
