package de.unihannover.talenteakademie.towerdefense.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marco on 28.07.15.
 */
public class SausageFont {

    private static final String FILE_FORMAT = "numbers/%c.png";

    public static List<Pixmap> getSprites(int number) {
        List<Pixmap> sprites = new ArrayList<>();

        String numberString = String.format("%04d", number);

        for (char c : numberString.toCharArray()) {
            sprites.add(new Pixmap(Gdx.files.internal(String.format(FILE_FORMAT, c))));
        }

        return sprites;
    }

}
