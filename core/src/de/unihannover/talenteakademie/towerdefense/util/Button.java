package de.unihannover.talenteakademie.towerdefense.util;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * A good old clickable button.
 */
@Data
public class Button {

    public static Texture SELECTION_TEXTURE = new Texture("selectbutton.png");

    private final Rectangle area;

    public Button(int x, int y, int width, int height) {
        this.area = new Rectangle(x, y, width, height);
    }

    @Getter @Setter
    public static class TowerButton extends Button {

        private final Class<? extends AbstractTower> towerClass;
        private boolean selected;

        public TowerButton(Class<? extends AbstractTower> towerClass, int x, int y, int width, int height) {
            super(x, y, width, height);

            this.towerClass = towerClass;
        }
    }
}
