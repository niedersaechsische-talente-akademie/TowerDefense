package de.unihannover.talenteakademie.towerdefense.util;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;

/**
 * Created by Marco on 28.07.15.
 */
@Data
public class Animation {

    private final int spriteDuration;
    private final Array<TextureRegion> sprites;

    @Getter(AccessLevel.NONE)
    private final Supplier<Vector2> vector2Supplier;

    private int lastSpriteTime, spriteNumber;

    private boolean finishedOnce = false;

    // === METHODS ===
    public TextureRegion getCurrentFrame(int timeMillis) {
        TextureRegion sprite = sprites.get(spriteNumber);

        if (timeMillis > lastSpriteTime + spriteDuration) {
            setSpriteNumber(spriteNumber + 1);
            lastSpriteTime = timeMillis;
        }

        return sprite;
    }

    public void setSpriteNumber(int newLastSpriteNumber) {
        spriteNumber = newLastSpriteNumber;
        if (spriteNumber >= sprites.size) {
            spriteNumber = 0;
            finishedOnce = true;
        }
    }

    public Vector2 getVector2() {
        return vector2Supplier.get();
    }

    public boolean queryFinished() {
        boolean tmp = finishedOnce;
        finishedOnce = false;
        return tmp;
    }
}
