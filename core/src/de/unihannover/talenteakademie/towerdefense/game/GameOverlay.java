package de.unihannover.talenteakademie.towerdefense.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import org.w3c.dom.Text;

import java.util.List;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.attacker.Wave;
import de.unihannover.talenteakademie.towerdefense.menu.MenuScreen;
import de.unihannover.talenteakademie.towerdefense.towers.impl.KnisterTower;
import de.unihannover.talenteakademie.towerdefense.towers.impl.MachineGunTower;
import de.unihannover.talenteakademie.towerdefense.towers.impl.MortarTower;
import de.unihannover.talenteakademie.towerdefense.towers.impl.ShotgunTower;
import de.unihannover.talenteakademie.towerdefense.util.Button;
import de.unihannover.talenteakademie.towerdefense.util.GameState;
import de.unihannover.talenteakademie.towerdefense.util.SausageFont;
import lombok.Getter;

/**
 * This class represents the overlay that is always visible and on top of all other graphics.
 */
public class GameOverlay {

    @Getter
    private final Button.TowerButton[] buttons = new Button.TowerButton[]{
            new Button.TowerButton(MachineGunTower.class, 67, 7, 142, 62),
            new Button.TowerButton(MortarTower.class, 240, 7, 142, 62),
            new Button.TowerButton(ShotgunTower.class, 414, 7, 142, 62),
            new Button.TowerButton(KnisterTower.class, 587, 7, 142, 62)
    };

    public Button.TowerButton getSelectedButton() {
        for (Button.TowerButton button : getButtons())
            if (button.isSelected()) return button;

        return null;
    }

    private GameModel game;

    private Button settingsButton;

    private BitmapFont defaultFont;
    private Texture overlayTexture;
    private Rectangle healthBar, progressBar;

    private final ShapeRenderer shapeRenderer = new ShapeRenderer();

    public GameOverlay(GameModel game) {
        this.game = game;

        settingsButton = new Button(731, 17, 57, 54);

        overlayTexture = new Texture("overlay.png");
        defaultFont = new BitmapFont(Gdx.files.internal("font/default.fnt"));
        defaultFont.getData().scaleX *= 2;
        defaultFont.getData().scaleY *= 2;

        // bottom-left (230 | 436)
        // top-right (617 | 470)
        Rectangle grayBar = new Rectangle(230, 436, 387, 34);

        healthBar = new Rectangle(grayBar.x, grayBar.y, grayBar.width, grayBar.height / 2);
        progressBar = new Rectangle(grayBar.x, grayBar.y + grayBar.height / 2, grayBar.width, grayBar.height / 2);
    }

    public void handleButtons(float x, float y) {
    }

    // The order of all function calls is very important. Consider twice when changing around.
    public void draw(SpriteBatch batch) {
        batch.end();

        float filledHealthBar = healthBar.width * game.getHealthPercentage();
        float filledProgressBar = progressBar.width * game.getProgressPercentage();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setProjectionMatrix(((GameScreen) GameState.RUNNING.getScreen()).getCamera().combined);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(healthBar.x, healthBar.y, filledHealthBar, healthBar.height);

        shapeRenderer.setColor(Color.BLUE);
        shapeRenderer.rect(progressBar.x, progressBar.y, filledProgressBar, progressBar.height);
        shapeRenderer.end();

        batch.begin();
        batch.draw(overlayTexture, 0, 0);

        if (getSelectedButton() != null)
            batch.draw(Button.SELECTION_TEXTURE,
                    getSelectedButton().getArea().getX(), getSelectedButton().getArea().getY());

        // Draw currency
        String currencyString = Integer.toString(game.getCurrency());
        int currencyWidth = 677 + 18 * (4 - currencyString.length());
        defaultFont.draw(batch, String.format("%s", currencyString), currencyWidth, 471);

        // Draw Wave number
        Pixmap pixmap = new Pixmap(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), Pixmap.Format.RGBA8888);
        if (game.getWave() != null) {
            List<Pixmap> sprites = SausageFont.getSprites(game.getWave().getId());
            for (int i = 0; i < sprites.size(); i++) {
                Pixmap sprite = sprites.get(i);
                Vector2 position = Wave.numberPoints.get(i);
                pixmap.drawPixmap(sprite, 0, 0, sprite.getWidth(), sprite.getHeight(),
                        (int) position.x, 480 - (int) position.y - sprite.getHeight() / 2, 16, 18);
                batch.draw(new Texture(pixmap), 0, 0);
            }
        }
    }
}
