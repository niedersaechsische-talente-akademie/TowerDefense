package de.unihannover.talenteakademie.towerdefense.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;
import de.unihannover.talenteakademie.towerdefense.towers.impl.KnisterTower;
import de.unihannover.talenteakademie.towerdefense.util.Animation;
import de.unihannover.talenteakademie.towerdefense.util.Button;
import de.unihannover.talenteakademie.towerdefense.util.GameState;
import de.unihannover.talenteakademie.towerdefense.towers.SelectableTower;
import de.unihannover.talenteakademie.towerdefense.util.Supplier;
import lombok.Getter;

/**
 * Created by Marco on 28.07.15.
 */
public class GameScreen extends ScreenAdapter {

    // === RENDERING & DISPLAY ===
    @Getter
    private OrthographicCamera camera;
    private SpriteBatch batch;

    // === TEXTURES & RESOURCES ===
    private GameOverlay overlay;
    private Texture gameOverOverlay;
    private Texture background;
    private List<Animation> animations = new ArrayList<>();

    // === DATA ===
    @Getter
    private GameModel gameModel;

    @Override
    public void show() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        gameModel = new GameModel();

        overlay = new GameOverlay(gameModel);
        background = new Texture("background.png");
        gameOverOverlay = new Texture("gameoveroverlay.png");

        loadAnimation(11, "flags/flag_%s.png", 120, new Supplier<Vector2>() {
            @Override
            public Vector2 get() {
                return new Vector2(269, 183);
            }
        }, true);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (!gameModel.isGameOver()) {
            handleTouchInput();
            gameModel.updateAttackers(delta);
            gameModel.updateTowers();
        }

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(background, 0, 80);

        drawWorld();

        overlay.draw(batch);

        batch.end();
    }

    private void handleTouchInput() {
        if (Gdx.input.justTouched()) {
            Vector3 v = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            float x = v.x, y = v.y;

            overlay.handleButtons(x, y);

            // Check for a button click
            for (Button.TowerButton button : overlay.getButtons()) {
                if (button.getArea().contains(x, y)) {
                    if (button.isSelected()) {
                        button.setSelected(false);
                        return;
                    }

                    for (Button.TowerButton otherButton : overlay.getButtons())
                        otherButton.setSelected(otherButton.equals(button));
                    return;
                }
            }

            // check for a selected tower
            for (AbstractTower tower : gameModel.getTowers()) {
                if (!(tower instanceof SelectableTower)) continue;
                SelectableTower selectable = (SelectableTower) tower;

                if (selectable.isSelected()) {
                    selectable.setTarget(new Vector2(x, y));
                    selectable.setSelected(false);
                    return;
                }
            }

            // No button clicked, check for tower destination click
            for (int i = 0; i < AbstractTower.TOWER_POSITIONS.length; i++) {
                Rectangle area = AbstractTower.TOWER_POSITIONS[i];
                if (!area.contains(x, y)) continue;

                // Tower position was selected / clicked

                // already placed tower && no tower was selected -> select it..
                AbstractTower tower = gameModel.getTower(i);
                if (tower != null) {
                    if ((tower instanceof SelectableTower)) {
                        SelectableTower selectable = (SelectableTower) tower;
                        selectable.setSelected(true);
                    }
                    return;
                }

                Button.TowerButton button = overlay.getSelectedButton();
                if (button == null) break; // no button selected

                try {
                    // check currency
                    Field costField = button.getTowerClass().getDeclaredField("COST");
                    costField.setAccessible(true);
                    int cost = (int) costField.get(null);
                    if (cost > gameModel.getCurrency()) return;


                    // Instantiate Tower object via reflection
                    Constructor[] constructors = button.getTowerClass().getConstructors();
                    Constructor idConstructor = null;
                    for (Constructor constructor : constructors) {
                        if (constructor.getGenericParameterTypes().length != 1) continue;
                        if (constructor.getParameterTypes()[0].equals(int.class))
                            idConstructor = constructor;
                    }

                    if (idConstructor != null) {
                        AbstractTower newTower = (AbstractTower) idConstructor.newInstance(i);
                        gameModel.addTower(i, newTower);
                        gameModel.pay(cost);
                        button.setSelected(false);

                        if (newTower instanceof SelectableTower) {
                            SelectableTower selectableTower = (SelectableTower) newTower;
                            selectableTower.setSelected(true);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("This shouldn't have happened! Please check your Tower classes!");
                }


                System.out.println(button.getTowerClass().getSimpleName() + " placed.");
                break;
            }
        }
    }

    private void drawWorld() {
        for (AbstractTower tower : gameModel.getTowers()) {
            if (tower == null) continue;
            batch.draw(tower.getTexture(), tower.getPosition().getX(), tower.getPosition().getY());
            tower.animate(batch);
        }

        for (Attacker attacker : gameModel.getAttackers()) {
            batch.draw(attacker.getTexture(), attacker.getPosition().x, attacker.getPosition().y);
        }

        for (Animation animation : animations)
            batch.draw(animation.getCurrentFrame(TowerDefenseGame.getInstance().getTimeMillis()),
                    animation.getVector2().x, animation.getVector2().y);

        if (gameModel.isGameOver()) batch.draw(gameOverOverlay, 0, 0);
    }

    // === ANIMATIONS ===
    private Animation loadAnimation(int frames, String format, int frameDuration,
                                    Supplier<Vector2> supplier, boolean animateConstantly) {
        Array<TextureRegion> textureRegions = new Array<>();

        for (int i = 1; i <= frames; i++) {
            textureRegions.add(new TextureRegion(new Texture(String.format(format, i))));
        }

        Animation animation = new Animation(frameDuration, textureRegions, supplier);

        if (animateConstantly) animations.add(animation);
        return animation;
    }

    public static Animation loadAnimation(int frames, String format, int frameDuration,
                                          Supplier<Vector2> supplier) {
        return ((GameScreen) GameState.RUNNING.getScreen())
                .loadAnimation(frames, format, frameDuration, supplier, false);
    }
}
