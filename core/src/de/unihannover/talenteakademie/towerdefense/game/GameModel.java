package de.unihannover.talenteakademie.towerdefense.game;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import de.unihannover.talenteakademie.towerdefense.attacker.AttackerType;
import de.unihannover.talenteakademie.towerdefense.attacker.Wave;
import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;
import lombok.Getter;

;

/**
 * Data class for the TowerDefenseGame game.
 */
@Getter
public class GameModel {

    // === INIT VALUES ===
    public static final int TOTAL_HEALTH = 300;
    public static final int START_CURRENCY = 150;

    // === MEMBERS ===
    private final Random random = new Random();

    public boolean gameOver = false;

    private int health, currency;
    private float progress;

    private Wave wave;
    private int waveStartTime;

    private AbstractTower[] towers = new AbstractTower[12];

    private List<Attacker> attackers = new ArrayList<>();

    public GameModel() {
        Wave.loadWaves();

        this.health = TOTAL_HEALTH;
        this.currency = START_CURRENCY;
        this.wave = Wave.getFirstWave();
        this.waveStartTime = TowerDefenseGame.getInstance().getTimeMillis();
        this.progress = 0;
    }

    public void updateAttackers(float delta) {
        if (wave == null) return;

        int deltaMillis = (int) (delta * 1000f);

        // Add new attackers based on the AttackerMap configuration
        Iterator<Map.Entry<Float, AttackerType>> entryIterator = wave.getAttackerMap().entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<Float, AttackerType> entry = entryIterator.next();
            int delayMillis = (int) (entry.getKey() * 1000);

            if (TowerDefenseGame.getInstance().getTimeMillis() > waveStartTime + delayMillis) {
                AttackerType type = entry.getValue();
                if (type.equals(AttackerType.WAVE_END)) {
                    if (attackers.isEmpty()) {
                        wave = wave.nextWave();
                        waveStartTime = TowerDefenseGame.getInstance().getTimeMillis();
                        progress = 0;
                    }
                    break;
                }

                // Spawn new attacker...
                Attacker.AttackerBuilder attackerBuilder = type.getAttackerBuilder();

                if (random.nextBoolean()) {
                    attackerBuilder.position(new Vector2(Attacker.BOTTOM));
                    attackerBuilder.direction(Attacker.BOTTOM_TO_TOP);
                } else {
                    attackerBuilder.position(new Vector2(Attacker.TOP));
                    attackerBuilder.direction(Attacker.TOP_TO_BOTTOM);
                }

                Attacker attacker = attackerBuilder.build();
                attackers.add(attacker);
                entryIterator.remove();

                int length = wave.getSize() - wave.getAttackerMap().size();
                progress = (float) length / (float) Math.max(1, wave.getSize() - 1);
            }
        }

        // Move attackers
        Iterator<Attacker> attackerIterator = attackers.iterator();
        while (attackerIterator.hasNext())

        {
            Attacker attacker = attackerIterator.next();

            // Has died ?
            if (attacker.getHealth() <= 0) {
                currency += attacker.getReward();
                attackerIterator.remove();
                continue;
            }

            // Has rached base ?
            if (attacker.isAtBase()) {
                attackerIterator.remove();
                this.health -= attacker.getDamage();
                if (health <= 0) { // check for death
                    gameOver = true;
                    health = 0;
                }
                continue;
            }

            attacker.move(deltaMillis);
        }

    }

    public void updateTowers() {
        for (AbstractTower tower : towers) {
            if (tower == null) continue;
            tower.update(attackers);
        }
    }

    public float getHealthPercentage() {
        return (float) health / (float) TOTAL_HEALTH;
    }

    public float getProgressPercentage() {
        return progress;
    }

    public AbstractTower getTower(int id) {
        return towers[id];
    }

    public void addTower(int id, AbstractTower tower) {
        towers[id] = tower;
    }

    public void pay(int money) {
        currency -= money;
    }
}
