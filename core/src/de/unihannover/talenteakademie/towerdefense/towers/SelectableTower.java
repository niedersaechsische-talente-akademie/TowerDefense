package de.unihannover.talenteakademie.towerdefense.towers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Marco on 29.07.15.
 */
@Getter
@Setter
public abstract class SelectableTower extends AbstractTower {

    protected Vector2 target = null;

    private boolean selected;

    protected SelectableTower(int id, int cooldownTime, int damagePerShot, int radius, int cost) {
        super(id, cooldownTime, damagePerShot, radius, cost);

        this.target = getPosition().getCenter(new Vector2());
    }

    // === ANIMATIONS ===
    private TextureRegion crosshair = new TextureRegion(new Texture("crosshair.png"));

    @Override
    public void animate(SpriteBatch batch) {
        if (isSelected()) batch.draw(crosshair, target.x - crosshair.getRegionWidth() / 2,
                target.y - crosshair.getRegionHeight() / 2);
    }
}
