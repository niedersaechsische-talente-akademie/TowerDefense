package de.unihannover.talenteakademie.towerdefense.towers.impl;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;

/**
 * Created by Marco on 27.07.15.
 */
public class MachineGunTower extends AbstractTower {

    // Single target, fast shooting

    private static final int COOLDOWN_TIME = 100;
    private static final int DAMAGE_PER_SHOT = 2;
    private static final int RADIUS = 300;

    private static final String FILE_NAME = "machineguntower.png";

    private static final int COST = 100;

    public MachineGunTower(int id) {
        super(id, COOLDOWN_TIME, DAMAGE_PER_SHOT, RADIUS, COST);

        this.texture = new Texture(FILE_NAME);
    }

    @Override
    public void shoot(List<Attacker> attackers) {
        Vector2 centerPos = this.getPosition().getCenter(new Vector2());

        Attacker first = null;
        for (Attacker attacker : attackers) {
            Vector2 attackerPos = attacker.getCenter();

            if (centerPos.dst2(attackerPos) < Math.pow(RADIUS, 2)) {
                if (first == null) first = attacker;
                if (attacker.getPosition().x < first.getPosition().x) first = attacker;
            }
        }

        if (first != null)
            first.damage(DAMAGE_PER_SHOT);
    }

    @Override
    public void animate(SpriteBatch batch) {

    }
}
