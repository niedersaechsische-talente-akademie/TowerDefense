package de.unihannover.talenteakademie.towerdefense.towers.impl;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import org.w3c.dom.Text;

import java.util.List;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;

/**
 * Represents the 4th tower type - the most expensive thus most powerful one.
 */
public class KnisterTower extends AbstractTower {

    // AoE damage - centered on tower

    private static final int COOLDOWN_TIME = 200;
    private static final int DAMAGE_PER_SHOT = 10;
    private static final int RADIUS = 100;

    private static final String FILE_NAME = "knistertower.png";

    private static final int COST = 400;

    public KnisterTower(int id) {
        super(id, COOLDOWN_TIME, DAMAGE_PER_SHOT, RADIUS, COST);

        this.texture = new Texture(FILE_NAME);
    }

    private Texture aura = new Texture("knisteraura.png");
    private boolean attackerInRange = false;

    @Override
    public void shoot(List<Attacker> attackers) {
        attackerInRange = false;
        Vector2 centerPos = this.getPosition().getCenter(new Vector2());

        for (Attacker attacker : attackers) {
            Vector2 attackerPos = attacker.getCenter();

            // Attacker inside range of tower ?
            if (centerPos.dst2(attackerPos) < Math.pow(RADIUS, 2)) {
                attacker.damage(DAMAGE_PER_SHOT);
                attackerInRange = true;
            }
        }
    }

    @Override
    public void animate(SpriteBatch batch) {
        if (attackerInRange)
            if (TowerDefenseGame.getInstance().getTimeMillis() % 500 < 250)
                batch.draw(aura, getPosition().x - 64, getPosition().y - 64);
    }
}
