package de.unihannover.talenteakademie.towerdefense.towers.impl;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import de.unihannover.talenteakademie.towerdefense.game.GameScreen;
import de.unihannover.talenteakademie.towerdefense.towers.SelectableTower;
import de.unihannover.talenteakademie.towerdefense.util.Animation;
import de.unihannover.talenteakademie.towerdefense.util.Supplier;

/**
 * Created by Marco on 27.07.15.
 */
public class MortarTower extends SelectableTower {

    // AoE damage - target position is to be chosen by player anywhere on the map

    private static final int COOLDOWN_TIME = 3000;
    private static final int DAMAGE_PER_SHOT = 90;
    private static final int RADIUS = -1;

    private static final String FILE_NAME = "mortartower.png";

    private static final int COST = 200;

    public MortarTower(int id) {
        super(id, COOLDOWN_TIME, DAMAGE_PER_SHOT, RADIUS, COST);

        this.texture = new Texture(FILE_NAME);
    }

    // === CUSTOMIZED IMPLEMENTATION ===
    private static final int EXPLOSION_RADIUS = 75;

    private boolean isAnimatingShot = false;
    private Animation explosion = GameScreen.loadAnimation(14, "explosion/E%s.png", 50, new Supplier<Vector2>() {
        @Override
        public Vector2 get() {
            return getTarget();
        }
    });

    @Override
    public void shoot(List<Attacker> attackers) {
        this.isAnimatingShot = true;

        if (target == null) return;
        for (Attacker attacker : attackers)
            if (getTarget().dst2(attacker.getCenter()) < Math.pow(EXPLOSION_RADIUS, 2))
                attacker.damage(DAMAGE_PER_SHOT);
    }

    @Override
    public void animate(SpriteBatch batch) {
        super.animate(batch);

        if (explosion.queryFinished()) this.isAnimatingShot = false;
        if (!isAnimatingShot) return;
        int timeMillis = TowerDefenseGame.getInstance().getTimeMillis();

        TextureRegion textureRegion = explosion.getCurrentFrame(timeMillis);
        batch.draw(textureRegion,
                explosion.getVector2().x - textureRegion.getRegionWidth() / 2,
                explosion.getVector2().y - textureRegion.getRegionHeight() / 2);
    }
}
