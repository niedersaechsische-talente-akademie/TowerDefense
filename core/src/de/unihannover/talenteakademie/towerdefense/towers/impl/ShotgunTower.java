package de.unihannover.talenteakademie.towerdefense.towers.impl;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import java.util.List;

import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import de.unihannover.talenteakademie.towerdefense.towers.AbstractTower;

/**
 * Created by Marco on 27.07.15.
 */
public class ShotgunTower extends AbstractTower {

    // Targets an attacker - the closest one to the end.
    // AoE damage in a small triangular section / an angle with probably 5°

    private static final int COOLDOWN_TIME = 2000;
    private static final int DAMAGE_PER_SHOT = 60;
    private static final int RADIUS = 200;

    private static final String FILE_NAME = "shotguntower.png";

    private static final int COST = 150;

    private static final double ANGLE_DEGREE = 5;

    public ShotgunTower(int id) {
        super(id, COOLDOWN_TIME, DAMAGE_PER_SHOT, RADIUS, COST);

        this.texture = new Texture(FILE_NAME);
    }

    @Override
    public void shoot(List<Attacker> attackers) {
        Vector2 centerPos = this.getPosition().getCenter(new Vector2());

        Attacker first = null;
        for (Attacker attacker : attackers) {
            Vector2 attackerPos = attacker.getCenter();

            if (centerPos.dst2(attackerPos) < Math.pow(RADIUS, 2)) {
                if (first == null) first = attacker;
                if (attacker.getPosition().x < first.getPosition().x) first = attacker;
            }
        }

        if (first == null) return;
        Vector2 targetPos = first.getCenter();

        for (Attacker attacker : attackers) {
            Vector2 attackerPos = attacker.getCenter();

            Vector2 ca = new Vector2(attackerPos.x - centerPos.x, attackerPos.y - centerPos.y).nor();
            Vector2 ct = new Vector2(targetPos.x - centerPos.x, targetPos.y - centerPos.y).nor();
            double angle = Math.acos(ca.dot(ct)) * MathUtils.radiansToDegrees;

            if (angle < ANGLE_DEGREE) {
                attacker.damage(DAMAGE_PER_SHOT);
            }
        }
    }

    @Override
    public void animate(SpriteBatch batch) {
    }
}
