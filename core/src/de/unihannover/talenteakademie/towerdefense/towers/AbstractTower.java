package de.unihannover.talenteakademie.towerdefense.towers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

import java.util.List;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import lombok.Getter;


/**
 * Represents any tower that is available in the game.
 */
@Getter
public abstract class AbstractTower {

    public static final Rectangle[] TOWER_POSITIONS = new Rectangle[]{
            new Rectangle(228, 368, 64, 64),
            new Rectangle(356, 322, 64, 64),
            new Rectangle(481, 367, 64, 64),
            new Rectangle(584, 242, 64, 64),
            new Rectangle(713, 282, 64, 64),
            new Rectangle(713, 190, 64, 64),
            new Rectangle(501, 104, 64, 64),
            new Rectangle(353, 145, 64, 64),
            new Rectangle(218, 104, 64, 64),
            new Rectangle(149, 241, 64, 64),
            new Rectangle(10, 284, 64, 64),
            new Rectangle(4, 189, 64, 64)
    };

    private final int id, cooldownTime, damagePerShot, radius, cost;

    private Rectangle position;

    protected Texture texture;

    private long lastShot;

    protected AbstractTower(int id, int cooldownTime, int damagePerShot, int radius, int cost) {
        this.id = id;
        this.cooldownTime = cooldownTime;
        this.damagePerShot = damagePerShot;
        this.radius = radius;
        this.cost = cost;

        this.position = TOWER_POSITIONS[id];
    }

    public void update(List<Attacker> attackerList) {
        long currentTime = TowerDefenseGame.getInstance().getTimeMillis();

        if (currentTime > lastShot + getCooldownTime()) {
            shoot(attackerList);
            lastShot = currentTime;
        }
    }

    public abstract void shoot(List<Attacker> attackers);

    public abstract void animate(SpriteBatch batch);
}
