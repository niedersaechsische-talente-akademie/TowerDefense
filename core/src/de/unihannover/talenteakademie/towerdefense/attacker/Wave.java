package de.unihannover.talenteakademie.towerdefense.attacker;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

/**
 * Created by Marco on 28.07.15.
 */
@Data
public class Wave {

    private static final List<Wave> WAVES = new ArrayList<>();

    private static void registerWave(WaveBuilder builder) {
        WAVES.add(builder.addAttacker(AttackerType.WAVE_END).build());
    }

    public static void loadWaves() {
        // Wave 1
        WaveBuilder b1 = Wave.builder().id(1);
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 5; j++) {
                b1.addAttacker(AttackerType.BASIC);
                b1.pause(2);
            }
            b1.pause(8);
        }
        b1.addAttacker(AttackerType.ADVANCED);
        registerWave(b1);

        // Wave 2
        WaveBuilder b2 = Wave.builder().id(2);
        for (int i = 0; i < 10; i++) {
            b2.addAttacker(AttackerType.BASIC);
            b2.pause(4);
        }
        b2.addAttacker(AttackerType.ADVANCED);
        b2.pause(5);
        b2.addAttacker(AttackerType.ADVANCED);
        b2.pause(4);
        b2.addAttacker(AttackerType.ADVANCED);
        registerWave(b2);

        // Wave 3
        WaveBuilder b3 = Wave.builder().id(3);
        for (int i = 0; i < 10; i++) {
            b3.addAttacker(AttackerType.ADVANCED);
            b3.pause(2);
        }
        registerWave(b3);

        // Wave 4
        WaveBuilder b4 = Wave.builder().id(4);
        for (int i = 0; i < 7; i++) {
            b4.addAttacker(AttackerType.RUNNER);
            b4.pause(2);
        }
        registerWave(b4);

        // Wave 5
        WaveBuilder b5 = Wave.builder().id(5);
        for (int i = 0; i < 2; i++) {
            b5.addAttacker(AttackerType.JUGGERNAUT);
            b5.pause(10);
        }
        b5.addAttacker(AttackerType.JUGGERNAUT);
        registerWave(b5);

        // Wave 6
        WaveBuilder b6 = Wave.builder().id(6);
        for (int i = 0; i < 5; i++) {
            b6.addAttacker(AttackerType.ADVANCED);
            b6.pause(0.5f);
        }
        for (int i = 0; i < 4; i++) {
            b6.addAttacker(AttackerType.RUNNER);
            b6.pause(0.5f);
        }
        for (int i = 0; i < 5; i++) {
            b6.addAttacker(AttackerType.ADVANCED);
            b6.pause(0.5f);
        }
        registerWave(b6);

        // Wave 7
        WaveBuilder b7 = Wave.builder().id(7);
        for (int i = 0; i < 4; i++) {
            b7.addAttacker(AttackerType.JUGGERNAUT);
            b7.pause(2.5f);
        }
        registerWave(b7);

        WAVES.add(Wave.builder().id(WAVES.size()).build());

        // === FINISHED ===
        waves = new ArrayList<>(WAVES);
    }

    @Getter
    public static List<Wave> waves = null;

    public static Wave getFirstWave() {
        for (Wave wave : waves)
            if (wave.getId() == 1) return wave;

        return waves.isEmpty() ? null : waves.get(0);
    }

    // WAVE NUMBER
    public static List<Vector2> numberPoints = Arrays.asList(
            new Vector2(154, 431),
            new Vector2(170, 431),
            new Vector2(186, 431),
            new Vector2(202, 431)
    );

    // === MEMBERS ===
    private final int id, size;
    private final Map<Float, AttackerType> attackerMap;

    public Wave(int id, Map<Float, AttackerType> map) {
        this.id = id;
        this.attackerMap = new HashMap<>(map);
        this.size = map.size();
    }

    public static WaveBuilder builder() {
        return new WaveBuilder();
    }

    public Wave nextWave() {
        for (Wave wave : waves)
            if (wave.getId() == this.id + 1) return wave;

        return null;
    }

    /////////////////////////////////
    //      WAVE BUILDER CLASS      /
    /////////////////////////////////
    @ToString
    public static class WaveBuilder {
        private int id;
        private float totalTime;
        private Map<Float, AttackerType> attackerMap = new HashMap<>();

        WaveBuilder() {
        }

        public Wave.WaveBuilder id(int id) {
            this.id = id;
            return this;
        }

        public Wave.WaveBuilder addAttacker(AttackerType attackerType) {
            this.attackerMap.put(totalTime, attackerType);
            return this;
        }

        public Wave.WaveBuilder pause(float pauseTime) {
            this.totalTime += pauseTime;
            return this;
        }

        public Wave build() {
            return new Wave(id, attackerMap);
        }
    }
}
