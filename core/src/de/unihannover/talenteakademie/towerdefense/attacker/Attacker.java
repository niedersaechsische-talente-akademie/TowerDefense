package de.unihannover.talenteakademie.towerdefense.attacker;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

import de.unihannover.talenteakademie.towerdefense.TowerDefenseGame;
import de.unihannover.talenteakademie.towerdefense.game.GameScreen;
import de.unihannover.talenteakademie.towerdefense.util.GameState;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

/**
 * Represents an attacker in the {@link TowerDefenseGame TowerDefense game}.
 */
@Data
@Builder
public class Attacker {

    public static final Vector2 BOTTOM = new Vector2(810, 70);
    public static final Vector2 TOP = new Vector2(810, 405);

    public static final Vector2 BOTTOM_TO_TOP = new Vector2(-1f, .36f);
    public static final Vector2 TOP_TO_BOTTOM = new Vector2(-1f, -.36f);

    private final int speed, reward, damage;
    private int health;

    private boolean atBase = false;

    @NonNull
    private Vector2 position;

    private final Vector2 direction;

    private final Texture texture;

    public Vector2 getCenter() {
        return new Vector2(position).add(texture.getWidth() / 2, texture.getHeight() / 2);
    }

    public void move(float deltaMillis) {
        if (atBase || this.position.x < 0 - texture.getWidth() || health < 0) {
            this.atBase = this.position.x < 0 - texture.getWidth();
            return;
        }

        float multiplier = deltaMillis * (float) speed / 1000f;

        position.add(direction.x * multiplier, direction.y * multiplier);
    }

    public void damage(int damage) {
        this.health -= damage;
    }
}
