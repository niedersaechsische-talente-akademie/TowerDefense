package de.unihannover.talenteakademie.towerdefense.attacker;

import com.badlogic.gdx.graphics.Texture;

import de.unihannover.talenteakademie.towerdefense.attacker.Attacker;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Created by Marco on 28.07.15.
 */
@Getter
@RequiredArgsConstructor
public enum AttackerType {

    BASIC(Attacker.builder()
            .health(100)
            .speed(25)
            .reward(10)
            .damage(5)
            .texture(new Texture("attackers/basic_1.png"))
    ),
    ADVANCED(Attacker.builder()
            .health(150)
            .speed(25)
            .reward(20)
            .damage(10)
            .texture(new Texture("attackers/advanced_1.png"))
    ),
    RUNNER(Attacker.builder()
            .health(100)
            .speed(50)
            .reward(30)
            .damage(15)
            .texture(new Texture("attackers/runner_1.png"))
    ),
    JUGGERNAUT(Attacker.builder()
            .health(1500)
            .speed(10)
            .reward(80)
            .damage(25)
            .texture(new Texture("attackers/juggernaut_1.png"))
    ),
    WAVE_END(null);

    private final Attacker.AttackerBuilder attackerBuilder;
}
